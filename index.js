const path = require('path');
const {Transform} = require('stream');
const {Buffer} = require('buffer');
const xmlJs = require('xml-js');
const Vinyl = require('vinyl');

class Sprite {
    svgObject = {
        declaration: {
            attributes: {
                version: '1.0',
                encoding: 'utf-8'
            }
        },
        elements: [
            {
                type: 'element',
                name: 'svg',
                attributes: {
                    xmlns: 'http://www.w3.org/2000/svg'
                }
            }
        ]
    };

    get defs() {
        if(!this._defs) {
            this._defs = {
                type: 'element',
                name: 'defs',
                elements: []
            };
            this.root.push(this._defs);
        }
        return this._defs.elements;
    }

    get root() {
        if(!this.svgObject.elements[0].elements) {
            this.svgObject.elements[0].elements = [];
        }
        return this.svgObject.elements[0].elements;
    }

    getNewId() {
        this.lastId = this.lastId || 1;
        return `_${this.lastId++}`;
    }
}

const availableSymbolAttributes = ['x', 'y', 'width', 'height', 'viewBox', 'preserveAspectRatio', 'class', 'style', 'refX', 'refY', 'lang', 'tabindex'];
class SpriteEntry {
    constructor(spriteObject, svgObject, id) {
        this.spriteObject = spriteObject;
        this.svgObject = svgObject;
        this.id = id;
        this.oldIds = {};

        this.createNewIds(this.svgObject.elements);
        this.replaceIds(this.svgObject.elements);
        this.moveDefsToParentObject(this.svgObject.elements);
        this.sanitizeSymbolAttributes();

        this.spriteObject.root.push(this.svgObject.elements[0]);
    }

    createNewIds(elements) {
        for(const element of elements) {
            if(element?.attributes?.id) {
                this.oldIds[element.attributes.id] = this.spriteObject.getNewId();
            }
            if(element?.elements?.length) {
                this.createNewIds(element.elements);
            }
        }
    }

    replaceIds(elements) {
        for(const element of elements) {
            if(element?.attributes) {
                if('xlink:href' in element?.attributes) {
                    element.attributes.href = element.attributes['xlink:href'];
                    delete element.attributes['xlink:href'];
                }
                for(const attributeName in element.attributes) {
                    if(attributeName === 'id') {
                        element.attributes.id = this.oldIds[element.attributes.id];
                    } else if(attributeName === 'href') {
                        let result = /^#(.*)$/.exec(element.attributes[attributeName]);
                        if(result && result[1] in this.oldIds) {
                            element.attributes[attributeName] = `#${this.oldIds[result[1]]}`;
                        }
                    } else {
                        let result = /^url\(['"]?#(.*)['"]?\)$/.exec(element.attributes[attributeName]);
                        if(result && result[1] in this.oldIds) {
                            element.attributes[attributeName] = `url(#${this.oldIds[result[1]]})`;
                        }
                    }
                }
            }
            if(element?.elements?.length) {
                this.replaceIds(element.elements);
            }
        }
    }

    moveDefsToParentObject(elements) {
        if(!Array.isArray(elements)) {
            return;
        }
        const removeIt = [];
        for (let i = 0; i < elements.length; i++) {
            this.moveDefsToParentObject(elements[i].elements);
            if (elements[i].type === 'element' && elements[i].name === 'defs' && elements[i]?.elements) {
                for (const childElement of elements[i].elements) {
                    this.spriteObject.defs.push(childElement);
                }
                removeIt.push(i);
            }
        }
        for (const i of removeIt.reverse()) {
            elements.splice(i, 1);
        }
    }

    sanitizeSymbolAttributes() {
        const symbolElementObject = this.svgObject.elements[0];
        symbolElementObject.name = 'symbol';
        for (const attrName in symbolElementObject?.attributes) {
            if (availableSymbolAttributes.indexOf(attrName) === (-1)) {
                delete symbolElementObject.attributes[attrName];
            }
        }
        if(!symbolElementObject.attributes) {
            symbolElementObject.attributes = {};
        }
        symbolElementObject.attributes.id = this.id;
        if(!symbolElementObject.attributes?.viewBox && symbolElementObject.attributes?.width && symbolElementObject.attributes?.height) {
            symbolElementObject.attributes.viewBox = `0 0 ${symbolElementObject.attributes.width} ${symbolElementObject.attributes.height}`;
        }
        delete symbolElementObject.attributes.width;
        delete symbolElementObject.attributes.height;
    }
}

class Transformer extends Transform {
    constructor(options) {
        super(options);
        this.svgSpriteObject = new Sprite();
        this.destFilename = path.resolve(options.destFilename || 'sprite.xml');
    }

    _transform(file, encoding, callback) {
        new SpriteEntry(this.svgSpriteObject, xmlJs.xml2js(file.contents.toString()), file.stem);
        callback();
    }

    _flush(callback){
        callback(null, new Vinyl({
            path: this.destFilename,
            contents: Buffer.from(xmlJs.js2xml(this.svgSpriteObject.svgObject))
        }));
    }
}

module.exports = (destFilename) => new Transformer({objectMode: true, destFilename});
